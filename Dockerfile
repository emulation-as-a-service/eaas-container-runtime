from ubuntu
workdir /root/build
copy build-setup .
run ./build-setup
copy . .
run ./build
run mkdir -p /opt/eaas-container-runtime && mv eaas-container-runtime.iso eaas-container-runtime.qcow eaas-container-disk.img example-job/example-job.qcow /opt/eaas-container-runtime
